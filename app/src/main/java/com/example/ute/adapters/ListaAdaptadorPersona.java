package com.example.ute.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ute.R;
import com.example.ute.sw.modelo.PersonaJS;

import java.util.List;

public class ListaAdaptadorPersona extends ArrayAdapter<PersonaJS> {

    private Context context;
    private List<PersonaJS> lista;
    public ListaAdaptadorPersona(Context context, List<PersonaJS> lista) {
        super(context, R.layout.persona_item, lista);
        this.context = context;
        this.lista=lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.persona_item, null);
        final PersonaJS pPersonaJS = lista.get(position);
        TextView txtnombres = item.findViewById(R.id.nombres);
        TextView txtapellidos = item.findViewById(R.id.apellidos);
        TextView txtcelular = item.findViewById(R.id.celular);
        TextView txtidentificacion = item.findViewById(R.id.identificacion);
        TextView txtdireccion = item.findViewById(R.id.direccion);


        txtnombres.setText(pPersonaJS.getNombres());
        txtapellidos.setText(pPersonaJS.getApellidos());
        txtcelular.setText(pPersonaJS.getCelular());
        txtidentificacion.setText(pPersonaJS.getIdentificacion());
        txtdireccion.setText(pPersonaJS.getDireccion());
        return item;
    }

}