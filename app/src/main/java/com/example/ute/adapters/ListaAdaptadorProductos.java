package com.example.ute.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ute.sw.modelo.ProductoJS;
import com.example.ute.R;

import com.example.ute.sw.modelo.ProductoJS;

import java.util.List;

public class ListaAdaptadorProductos extends ArrayAdapter<ProductoJS> {
    private Context context;
    private List<ProductoJS> lista;

    public ListaAdaptadorProductos( Context context,  List<ProductoJS> lista) {
        super(context, R.layout.lista_item,lista);
        this.context = context;
        this.lista=lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.lista_item, null);

        final ProductoJS podProductoJS = lista.get(position);
        TextView txtnombre = item.findViewById(R.id.nombre);
        TextView precio = item.findViewById(R.id.precio);
        txtnombre.setText(podProductoJS.getNombre());
        precio.setText(podProductoJS.getP_venta().toString());
        return item;
    }

}