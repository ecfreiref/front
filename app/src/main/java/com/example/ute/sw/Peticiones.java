package com.example.ute.sw;

import android.content.Context;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.example.ute.sw.modelo.ListPersonaJS;
import com.example.ute.sw.modelo.ListaProductoJS;
import  com.example.ute.sw.VolleyRequest;
import java.util.List;

public class Peticiones {

public static String API_URL = "http://localhost:4000/api/v1/";

    public static VolleyRequest<ListaProductoJS> getListaProductos(
            @NonNull final Context context,
            @NonNull Response.Listener<ListaProductoJS> response,
            @NonNull Response.ErrorListener errorListener
    ){
        final String url = API_URL + "producto";
        VolleyRequest request = new VolleyRequest(
                context,
                Request.Method.GET,
                url,
                response, errorListener
        );
        request.setResponseClass(ListaProductoJS.class);
        return request;
    }

    public static VolleyRequest<ListPersonaJS> getListaPersonas(
            @NonNull final Context context,
            @NonNull Response.Listener<ListPersonaJS> response,
            @NonNull Response.ErrorListener errorListener
    ){
        String url = API_URL + "persona";
        VolleyRequest request = new VolleyRequest(
                context,
                Request.Method.GET,
                url,
                response, errorListener
        );
        request.setResponseClass(ListPersonaJS.class);
        return request;
    }
}
