package com.example.ute.sw.modelo;

public class ProductoJS {
    private String nombre;
    private Double p_compra;
    private Double p_venta;
    private Boolean estado;
    private String external_id;
    private Integer id_categoria;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getP_compra() {
        return p_compra;
    }

    public void setP_compra(Double p_compra) {
        this.p_compra = p_compra;
    }

    public Double getP_venta() {
        return p_venta;
    }

    public void setP_venta(Double p_venta) {
        this.p_venta = p_venta;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public Integer getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Integer id_categoria) {
        this.id_categoria = id_categoria;
    }
}
