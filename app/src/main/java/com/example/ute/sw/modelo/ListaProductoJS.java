package com.example.ute.sw.modelo;

import java.util.ArrayList;
import java.util.List;

public class ListaProductoJS {
    private String esg;
    private Integer code;
    private List<ProductoJS> data = new ArrayList();

    public String getEsg() {
        return esg;
    }

    public void setEsg(String esg) {
        this.esg = esg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<ProductoJS> getData() {
        return data;
    }

    public void setData(List<ProductoJS> data) {
        this.data = data;
    }
}
