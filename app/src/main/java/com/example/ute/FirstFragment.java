package com.example.ute;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ute.adapters.ListaAdaptadorPersona;
import com.example.ute.adapters.ListaAdaptadorProductos;
import com.example.ute.databinding.FragmentFirstBinding;
import com.example.ute.sw.Peticiones;
import com.example.ute.sw.VolleyRequest;
import com.example.ute.sw.modelo.ListPersonaJS;
import com.example.ute.sw.modelo.ListaProductoJS;
import com.example.ute.sw.modelo.PersonaJS;
import com.example.ute.sw.modelo.ProductoJS;

import java.util.ArrayList;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;
    private ListaAdaptadorProductos listaAdaptadorProducto;
    private ListaAdaptadorPersona listaAdaptadorPersona;
    private ListView lista;
    private ListView listaPersona;
    private RequestQueue requestQueue;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        requestQueue = Volley.newRequestQueue(getContext());
        listaAdaptadorProducto = new ListaAdaptadorProductos(getContext(), new ArrayList<ProductoJS>());
        listaAdaptadorPersona = new ListaAdaptadorPersona(getContext(), new ArrayList<PersonaJS>());
        lista = view.findViewById(R.id.lista);
        // PRODUCTO

//        lista.setEmptyView(view.findViewById(R.id.listaVacia1));
//        cargarProducto();

        // PERSONA
        listaPersona = view.findViewById(R.id.listapersona);
        listaPersona.setEmptyView(view.findViewById(R.id.listaVaciaPersona));
        cargarPersona();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void cargarProducto(){
        VolleyRequest<ListaProductoJS> peticion = Peticiones.getListaProductos(getContext(), new Response.Listener<ListaProductoJS>(){
                    @Override
                    public void onResponse(ListaProductoJS response){
                        if(response.getCode() == 200){
                            System.out.println("Si llega producto");
                            listaAdaptadorProducto = new ListaAdaptadorProductos(getContext(), response.getData());
                            lista.setAdapter(listaAdaptadorProducto);
                        }
                        System.out.println("Responder...");
                    }
                },
                new Response.ErrorListener(){
                    public void onErrorResponse(VolleyError error){
                        System.out.println("Error de llegada, productos"+ error);
                    }
                });
        requestQueue.add(peticion);
    }

    private void cargarPersona(){
        VolleyRequest<ListPersonaJS> peticion = Peticiones.getListaPersonas(getContext(), new Response.Listener<ListPersonaJS>() {
                    @Override
                    public void onResponse(ListPersonaJS response) {
                        if(response.getCode() == 200){
                            System.out.println("Si llego la persona");
                            listaAdaptadorPersona = new ListaAdaptadorPersona(getContext(), response.getData());
                            listaPersona.setAdapter(listaAdaptadorPersona);
                        }
                        System.out.println("Esta en responder persona!");
                    }
                },
                new Response.ErrorListener(){
                    public void onErrorResponse(VolleyError error){
                        System.out.println("Error de llegada, personas: "+ error);
                    }
                }) ;
        requestQueue.add(peticion);
    }

}